import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.css']
})
export class DocumentComponent implements OnInit {

  /**
   * @description 컴플라이 설명 타겟, assets/images/document 폴더 내 이미지명과 일치해야함.
   * @memberof DocumentComponent
   */
  visible_target = '';
  constructor(
    private route: ActivatedRoute
  ) { }

  /**
   * @description 컴플라이 설명 주소 파싱
   * @memberof DocumentComponent
   */
  ngOnInit() {
    this.route.params.subscribe(
      param => {
        this.visible_target = param['document_target'];
      });
  }
}
