import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MatchComponent } from './match.component';
import { SearchComponent } from './search/search.component';
import { DisplayComponent } from './display/display.component';
import { ElasticScrollComponent } from './display/elastic-scroll/elastic-scroll.component';

import { AnimationStateService } from './animation-state.service';
import { SearchService } from './search/search.service';
import { ProductService } from './product.service';
import { EartipListResolve } from './display/eartip-list.resolve';
import { EartipFigureComponent } from './display/eartip-figure/eartip-figure.component';
import { EartipComponent } from './display/eartip-figure/eartip/eartip.component';

import { ProductResolve } from './display/product-resolve';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      {
        path: '',
        component: MatchComponent,
        children: [
          {
            path: ':model_id',
            component: DisplayComponent,
            resolve: {
              product : ProductResolve,
              eartip_list: EartipListResolve
            }
          }
        ]
      }
    ])
  ],
  declarations: [MatchComponent, SearchComponent, DisplayComponent, ElasticScrollComponent, EartipFigureComponent, EartipComponent],
  providers: [AnimationStateService, SearchService, ProductService, EartipListResolve, ProductResolve]
})
export class MatchModule { }
