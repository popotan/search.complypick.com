import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { handleError } from '../../async-handling.observable';
import { apiURL } from '../../environments/environment.prod';

@Injectable()
export class ProductService {
  target: any | undefined;

  constructor(
    private $http: Http
  ) { }

  getProductInfo(model_id) {
    this.target = undefined;
    return this.$http.get( apiURL + '/' + model_id)
      .map((res: Response) => res.json()).catch(handleError);
  }

}
