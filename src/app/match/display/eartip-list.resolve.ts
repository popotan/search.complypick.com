import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { handleError } from '../../../async-handling.observable';
import { apiURL } from '../../../environments/environment.prod';

@Injectable()
export class EartipListResolve implements Resolve<any> {

  constructor(private $http: Http,
    private router: Router) { }

  resolve(route: ActivatedRouteSnapshot) {
    let product_id = +route.params['model_id'];
    return new Promise((resolve, reject) => {
      this.getEartipList(product_id)
        .subscribe(
          result => {
            if (result['message'] === 'SUCCESS') {
              resolve(result['response']);
            } else {
              reject(false);
            }
          });
    });
  }

  getEartipList(product_id) {
    return this.$http.get(apiURL + '/' + product_id + '/eartip')
      .map((res: Response) => res.json()).catch(handleError);
  }
}
