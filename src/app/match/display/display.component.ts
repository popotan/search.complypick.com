import { SafeUrl, DomSanitizer, Title, Meta } from '@angular/platform-browser';
import { Component, OnInit, Input, ElementRef, AfterViewInit, AfterViewChecked, ViewChild } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router, NavigationEnd } from '@angular/router';

import { SearchService } from '../search/search.service';
import { AnimationStateService } from '../animation-state.service';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css'],
  animations: [trigger('display-state', [
    state('search', style({ right: '-100%' })),
    state('display', style({ right: 0 })),
    transition('search <=> display', animate('300ms ease-in-out')),
    transition('void => *', animate('300ms ease-in-out'))
  ]), trigger('article-state', [
    state('show', style({ height: 'calc(100% - 50px - 50px)' })),
    state('hide', style({ height: '30%' })),
    transition('show <=> hide', animate('300ms ease-in-out')),
    transition('void => *', animate('300ms ease-in-out'))
  ])]
})
export class DisplayComponent implements OnInit, AfterViewInit {
  /**
   * @description 설명 노출상태
   * @type {String}
   * @memberof DisplayComponent
   */
  article: String = 'hide';

  /**
   * @description 사용가능 제품 목록
   * @type {(any[] | undefined)}
   * @memberof DisplayComponent
   */
  _productList: any[] | undefined;

  /**
   * @description 현재 노출(가운데 위치)한 제품의 index
   * @type {number}
   * @memberof DisplayComponent
   */
  articleIndex: number;
  @ViewChild('noNickName') noNickName;
  constructor(
    public el: ElementRef,
    private searchService: SearchService,
    public route: ActivatedRoute,
    private router: Router,
    public animationState: AnimationStateService,
    public model: ProductService,
    public sanitizer: DomSanitizer,
    private titleService: Title,
    private metaService: Meta
  ) {
    route.params.subscribe(
      params => {
        if (params.hasOwnProperty('model_id')) {
          animationState.state = 'display';
          animationState.show_no_selected_product = false;
        } else {
          animationState.state = 'search';
          animationState.show_no_selected_product = true;
        }
      });
    route.data.subscribe(
      result => {
        this.productList = result.eartip_list;
        model.target = result.product;
        setTimeout(() => {
          const pageTitle: string = this.model.target['name'] + ' 폼팁 호환리스트';
          titleService.setTitle(pageTitle);
          metaService.removeTag('property="og:title"');
          metaService.addTag({ property: 'og:title', content: pageTitle });
          metaService.removeTag('property="og:image"');
          metaService.addTag({
            property: 'og:image', content: 'http://static.soundpick.net/eartip/product/' + this.model.target['image_file'] });
          metaService.removeTag('property="og:url"');
          metaService.addTag({ property: 'og:url', content: 'http://search.complypick.com/match/' + this.model.target['id'] });
        });
      });
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  set productList(value: any[]) {
    this._productList = value;
  };
  get productList() {
    return this._productList;
  }

  sanitizeUrl(url) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
}
