import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter, ElementRef, ViewChildren, QueryList } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { ElasticScrollComponent } from '../elastic-scroll/elastic-scroll.component';
@Component({
  selector: 'app-eartip-figure',
  templateUrl: './eartip-figure.component.html',
  styleUrls: ['./eartip-figure.component.css']
})
export class EartipFigureComponent extends ElasticScrollComponent implements OnInit {

  /**
   *
   *
   * @type {*}
   * @memberof EartipFigureComponent
   */
  _productList: any;
  
  /**
   *Creates an instance of EartipFigureComponent.
   * @param {ElementRef} el
   * @param {ActivatedRoute} route
   * @memberof EartipFigureComponent
   */
  constructor(
    public el: ElementRef,
    public route: ActivatedRoute
  ) {

    super(el, route);
  }

  ngOnInit() {
  }

  /**
   * @description 사용 가능한 이어팁 리스트, 리스트 갱신되면 초기화하고, child 컴포넌트들로부터 element 제어권한 받아옴
   * @memberof EartipFigureComponent
   */
  @Input() set productList(value) {
    super.resetEartips();
    super.eartipListAdmin(this.el);
    super.onChangeRoute();
    this._productList = value;
  }

  /**
   * @description EartipComponent element를 전파, 모두 전파할 때까지 반복하여 실행한다.
   * @param {ElementRef} $event
   * @memberof EartipFigureComponent
   */
  receiveChildEartip($event: ElementRef) {
    super.eartipsAdmin($event, this._productList.length);
  }
}