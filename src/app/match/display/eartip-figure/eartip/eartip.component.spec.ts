import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EartipComponent } from './eartip.component';

describe('EartipComponent', () => {
  let component: EartipComponent;
  let fixture: ComponentFixture<EartipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EartipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EartipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
