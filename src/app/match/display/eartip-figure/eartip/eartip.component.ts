import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter, ElementRef } from '@angular/core';

@Component({
  selector: 'app-eartip',
  templateUrl: './eartip.component.html',
  styleUrls: ['./eartip.component.css']
})
export class EartipComponent implements OnInit, AfterViewInit {

  /**
   * @description 노출하고자 하는 이어팁 정보
   * @type {*}
   * @memberof EartipComponent
   */
  @Input() tip: any;

  /**
   * @description 상위 컴포넌트에 element 제어권 전파를 위한 EventEmitter
   * @memberof EartipComponent
   */
  @Output() pushMyChildElement = new EventEmitter<any>();
  constructor(
    public el: ElementRef
  ) { }

  ngOnInit() {
  }

  /**
   * @description 상위 컴포넌트에 element 제어권 전파
   * @memberof EartipComponent
   */
  ngAfterViewInit() {
    this.pushMyChildElement.emit(this.el);
  }
}
