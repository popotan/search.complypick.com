import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EartipFigureComponent } from './eartip-figure.component';

describe('EartipFigureComponent', () => {
  let component: EartipFigureComponent;
  let fixture: ComponentFixture<EartipFigureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EartipFigureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EartipFigureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
