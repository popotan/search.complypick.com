import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { handleError } from '../../../async-handling.observable';

@Injectable()
export class ProductResolve {

  constructor(private $http: Http,
    private router: Router) { }

  resolve(route: ActivatedRouteSnapshot) {
    let product_id = +route.params['model_id'];
    return new Promise((resolve, reject) => {
      this.getProduct(product_id)
        .subscribe(
          result => {
            if (result['message'] === 'SUCCESS') {
              resolve(result['response']);
            } else {
              reject(false);
            }
          })
    });
  }

  getProduct(product_id) {
    return this.$http.get('/api/' + product_id)
      .map((res: Response) => res.json()).catch(handleError);
  }
}
