import { TestBed, inject } from '@angular/core/testing';

import { EartipListService } from './eartip-list.service';

describe('EartipListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EartipListService]
    });
  });

  it('should be created', inject([EartipListService], (service: EartipListService) => {
    expect(service).toBeTruthy();
  }));
});
