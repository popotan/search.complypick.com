import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Inject, AfterViewInit, ViewChild, ViewChildren, QueryList, ElementRef, HostListener } from '@angular/core';
import { ActivatedRoute, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-elastic-scroll',
  templateUrl: './elastic-scroll.component.html',
  styleUrls: ['./elastic-scroll.component.css']
})
export class ElasticScrollComponent implements OnInit, AfterViewInit {

  /**
   * @description 스크롤을 제어할 element
   * EartipFigureComponent로부터 전파받은 element
   * @type {ElementRef}
   * @memberof ElasticScrollComponent
   */
  eartip_list: ElementRef;

  /**
   * @description 스크롤 할 대상 elements
   * 각 EartipComponent로부터 전파받은 element
   * @type {ElementRef[]}
   * @memberof ElasticScrollComponent
   */
  eartips: ElementRef[] = [];

  /**
   * @description eartips property에 있는 element의 중간위치값
   * @memberof ElasticScrollComponent
   */
  eartip_center_arr = [];

  /**
   * @description 스크롤 중심값을 잡기 위한 브라우저 중심 좌표
   * @memberof ElasticScrollComponent
   */
  window_width = window.screenX;

  /**
  * @description 마우스/터치 움직임 상태를 추적하기 위한 property
  * @memberof ElasticScrollComponent
  */
  mouse_event = {
    is_clicked: false,
    prev_pageX_touch: 0,
    element_left_pos: 0
  };

  /**
   * @description elastic-scroll 완료 후, 현재 인덱스 상위 컴포넌트에 전파
   * @memberof ElasticScrollComponent
   */
  @Output() emitArticleIndex = new EventEmitter<number>();

  constructor(
    public el: ElementRef,
    public route: ActivatedRoute
  ) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
  }

  /**
   * @description 스크롤을 제어할 element를 property에 할당
   * @param {*} $element
   * @memberof ElasticScrollComponent
   */
  eartipListAdmin($element) {
    this.eartip_list = $element;
  }

  /**
   * @description 스크롤 할 대상 elements를 property에 할당
   * @param {*} $element
   * @param {*} totalLength
   * @memberof ElasticScrollComponent
   */
  eartipsAdmin($element, totalLength) {
    this.eartips.push($element);
    if (this.eartips.length === totalLength) {
      this.InitEartipsPosition();
      // for (let i = 0; i < this.eartips.length; i++) {
      //   if (this.eartips[i].nativeElement.innerText.indexOf('Sport') > -1) {
      //     // setTimeout(() => {
      //     //   this.moveTo(i);
      //     // }, 800);
      //     break;
      //   }
      // }
    }
  }

  /**
   * @description 객체정보 변경 시, 초기화 method
   * @memberof ElasticScrollComponent
   */
  resetEartips() {
    this.eartips = [];
    this.eartip_center_arr = [];
  }

  /**
   * @description 초기화 시, 각 스크롤 대상 element의 중간값 계산하고, 가까운 인덱스로 elastic-scrolling
   * @memberof ElasticScrollComponent
   */
  InitEartipsPosition() {
    this.eartips.forEach(element => {
      this.eartip_center_arr.push(element.nativeElement.offsetLeft + (element.nativeElement.offsetWidth / 2));
    });
    this.onMouseActionEnd();
  }

  /**
   * @description 경로 변경 시, 위치 초기화
   * @memberof ElasticScrollComponent
   */
  onChangeRoute() {
    this.eartip_list.nativeElement.style.left = 0;
    this.mouse_event.element_left_pos = 0;
  }

  /**
   * @description 브라우저 중심 좌표 추적하고, 스크롤 이동
   * @param {*} $event
   * @memberof ElasticScrollComponent
   */
  @HostListener('window:resize', ['$event'])
  onWindowResize($event) {
    this.onMouseActionEnd();
  }

  /**
   * @description 최초 x좌표를 추적하고, 애니메이션 밀림 현상 방지 위해 transition off
   * @param {MouseEvent} $event
   * @memberof ElasticScrollComponent
   */
  @HostListener('touchstart', ['$event'])
  @HostListener('mousedown', ['$event'])
  onMouseDown($event: MouseEvent) {
    if (this.eartip_list && this.eartips) {
      if ($event.type === 'touchstart') {
        this.mouse_event.prev_pageX_touch = $event.pageX;
      }
      this.mouse_event.is_clicked = true;
      this.eartip_list.nativeElement.style.transition = 'none';
    }
  }

  /**
   * @description x좌표를 마우스/터치 움직임만큼 이동(가속)
   * @param {MouseEvent} $event
   * @memberof ElasticScrollComponent
   */
  @HostListener('touchmove', ['$event'])
  @HostListener('mousemove', ['$event'])
  onMouseMove($event: MouseEvent) {
    if (this.mouse_event.is_clicked) {
      let movement = 0;
      if ($event.type === 'touchmove') {
        //
        $event.preventDefault();
        movement = ($event.pageX - this.mouse_event.prev_pageX_touch) * 1.9;
        this.mouse_event.prev_pageX_touch += movement;
      } else {
        movement = $event.movementX;
      }
      for (let i = 0; i < this.eartip_center_arr.length; i++) {
        this.eartip_center_arr[i] += movement;
      }
      this.mouse_event.element_left_pos += movement;
      this.eartip_list.nativeElement.style.left = Math.floor(this.mouse_event.element_left_pos).toString() + 'px';
    }
  }

  /**
   * @description 커서가 브라우저 밖을 벗어날 때
   * @param {MouseEvent} $event
   * @memberof ElasticScrollComponent
   */
  @HostListener('mouseleave', ['$event'])
  onMouseOut($event: MouseEvent) {
    if (this.mouse_event.is_clicked) {
      this.onMouseActionEnd();
    }
  }

  /**
   * @param {MouseEvent} $event
   * @memberof ElasticScrollComponent
   */
  @HostListener('mouseup', ['$event'])
  onMouseUp($event: MouseEvent) {
    if (this.mouse_event.is_clicked) {
      this.onMouseActionEnd();
    }
  }

  /**
   * @param {*} $event
   * @memberof ElasticScrollComponent
   */
  @HostListener('touchend', ['$event'])
  ontouchend($event) {
    if (this.mouse_event.is_clicked) {
      this.mouse_event.prev_pageX_touch = 0;
      this.onMouseActionEnd();
    }
  }

  /**
   * @description 클릭/드래그 종료 후, x좌표값에 가장 가까운 중심값을 가진 엘리먼트로 elastic-scroll 이동
   * @returns
   * @memberof ElasticScrollComponent
   */
  onMouseActionEnd() {
    this.mouse_event.is_clicked = false;
    this.eartip_list.nativeElement.style.transition = '0.3s';
    let pCenter = window.innerWidth / 2;
    for (var i = 0; i < this.eartip_center_arr.length; i++) {
      let margin = pCenter - this.eartip_center_arr[i]; //플러스는 왼쪽, 마이너스는 오른쪽
      if (margin < 0) { //마이너스, 오른쪽이면
        if (i > 0) {
          margin = Math.abs(margin);
          let before_margin = Math.abs(pCenter - this.eartip_center_arr[i - 1]);
          if (before_margin < margin) {
            this.emitArticleIndex.emit(i - 1);
            return this.moveElastically('right', before_margin);
          } else if (before_margin === margin) {
            this.emitArticleIndex.emit(i);
            return this.moveElastically('left', before_margin);
          } else {
            this.emitArticleIndex.emit(i);
            return this.moveElastically('left', margin);
          }
        } else {
          this.emitArticleIndex.emit(i);
          return this.moveElastically('left', Math.abs(margin));
        }
      }
    }
    this.emitArticleIndex.emit(i - 1);
    return this.moveElastically('right', pCenter - this.eartip_center_arr[i - 1]);
  }

  moveElastically(direction: string, margin: number) {
    if (direction === 'left') {
      this.mouse_event.element_left_pos -= margin;
      this.eartip_list.nativeElement.style.left = this.mouse_event.element_left_pos.toString() + 'px';
      for (let i = 0; i < this.eartip_center_arr.length; i++) {
        this.eartip_center_arr[i] -= margin;
      }
    } else if (direction === 'right') {
      this.mouse_event.element_left_pos += margin;
      this.eartip_list.nativeElement.style.left = this.mouse_event.element_left_pos.toString() + 'px';
      for (let i = 0; i < this.eartip_center_arr.length; i++) {
        this.eartip_center_arr[i] += margin;
      }
    }
  }

  /**
   * @description 특정 index로 이동
   * @param {number} index
   * @memberof ElasticScrollComponent
   */
  moveTo(index: number) {
    this.mouse_event.is_clicked = false;
    this.eartip_list.nativeElement.style.transition = '0.3s';
    this.eartip_list.nativeElement.style.left = ((this.window_width / 2) - this.eartip_center_arr[index]).toString() + 'px';
    // for (var i = 0; i < this.eartip_center_arr.length; i++) {
    //   this.eartip_center_arr[i] -= this.eartip_center_arr[index];
    // }
    this.emitArticleIndex.emit(index);
  }
}