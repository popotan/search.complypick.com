import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElasticScrollComponent } from './elastic-scroll.component';

describe('ElasticScrollComponent', () => {
  let component: ElasticScrollComponent;
  let fixture: ComponentFixture<ElasticScrollComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElasticScrollComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElasticScrollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
