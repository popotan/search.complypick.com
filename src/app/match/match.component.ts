// import 'rxjs/add/operator/pairwise';
import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

import { AnimationStateService } from './animation-state.service';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.css'],
  animations: [trigger('search-state', [
    state('search', style({ left: 0, opacity: 1 })),
    state('display', style({ left: '-100%', opacity: 0 })),
    state('void', style({ left: '-100%' })),
    transition('search <=> display', animate('300ms ease-in-out')),
    transition('void => *', animate('300ms ease-in-out'))
  ])]
})
export class MatchComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public animationState: AnimationStateService
  ) {
  }

  ngOnInit() {
    this.animationState.state = 'search';
    this.animationState.searchState = 'init';
    this.animationState.show_no_selected_product = true;
  }
}
