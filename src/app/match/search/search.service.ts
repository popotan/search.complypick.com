import { Injectable } from '@angular/core';

import { Http, URLSearchParams, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { handleError } from '../../../async-handling.observable';
import { apiURL } from '../../../environments/environment.prod';

@Injectable()
export class SearchService {

  constructor(
    private $http: Http
  ) { }

  getEarphoneSearchResult(params) {
    return this.$http.get(apiURL + '/search', { params: params })
      .map((res: Response) => res.json()).catch(handleError);
  }
}
