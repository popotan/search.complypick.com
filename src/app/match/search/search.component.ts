import 'rxjs/add/operator/pairwise';

import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';

import { SearchService } from './search.service';
import { AnimationStateService } from '../animation-state.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  animations: [trigger('searchResultAnimate', [
    state('void', style({ opacity: 0 })),
    state(':enter', style({ opacity: 1 }))
  ]), trigger('search-position', [
    state('init', style({ height: '100%' })),
    state('searching', style({ height: '100px', boxShadow: '1px 2px 3px 0 rgba(0, 0, 0, .33)' })),
    transition('init <=> searching', animate('200ms ease-in'))
  ]), trigger('result-background', [
    state('init', style({ background: '#fff' })),
    state('searching', style({ background: 'hsla(0, 0%, 95%, .87)' })),
    transition('init <=> searching', animate('200ms ease-in'))
  ])]
})
export class SearchComponent implements OnInit, AfterViewInit {

  /**
   * @description 안내메세지 노출 여부
   * @memberof SearchComponent
   */
  switch = {
    no_search_result: false,
    is_searching: false
  };

  /**
   * @description 검색옵션 및 결과
   * @memberof SearchComponent
   */
  search = {
    result: [],
    option: {
      keyword: ''
    }
  };

  /**
   * @description 검색어를 observable하게 받아오기 위한 검색어 subject class
   * @memberof SearchComponent
   */
  keywordSubject = new Subject<string>();

  constructor(
    public animationState: AnimationStateService,
    private searchService: SearchService,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.keywordSubject.debounceTime(500)
      .subscribe(value => {
        if (value.length > 1) {
          this.animationState.searchState = 'searching';
          this.search.option.keyword = value;
          this.getSearchResult();
        } else {
          this.animationState.searchState = 'init';
          this.search.option.keyword = value;
          this.search.result = [];
          this.switch.no_search_result = true;
        }
      });
  }

  windowTopScroll($event) {
    window.scrollTo(0, 0);
  }

  /**
   * @description keyup 시, keyword subject에 값 전달
   * @param {*} $event
   * @memberof SearchComponent
   */
  onChangeSearchKeyword($event) {
    this.keywordSubject.next($event.target.value);
  }

  /**
   * @description 제품검색결과 획득
   * @memberof SearchComponent
   */
  getSearchResult() {
    this.switch.is_searching = true;
    this.searchService.getEarphoneSearchResult(this.search.option)
      .subscribe(
        result => {
          if (result['message'] === 'SUCCESS') {
            this.search.result = result['response'];
            this.switch.no_search_result = false;
          } else if (result['message'] === 'NO_KEYWORD') {
            this.search.result = [];
            this.switch.no_search_result = true;
          }
        }, error => {

        }, () => {
          this.switch.is_searching = false;
        });
  }

  /**
   * @description 제품 선택 시, 사용가능 제품 리스트로 이동
   * @param {number} model_id
   * @memberof SearchComponent
   */
  display(model_id: number) {
    this.router.navigate(['/match', model_id])
      .then(result => {
        this.animationState.state = 'display';
      });
  }

}