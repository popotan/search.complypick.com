import { Component } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(
    public router: Router,
    public titleService: Title,
    public metaService: Meta
  ) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        let title_arr = this.getTitle(router.routerState, router.routerState.root);
        titleService.setTitle(title_arr[0]);

        let meta_arr = this.getMeta(router.routerState, router.routerState.root);
        metaService.addTags(meta_arr[0]);
      }
    });
  }

  getTitle(state, parent) {
    let data = [];
    if (parent && parent.snapshot.data && parent.snapshot.data.title) {
      data.push(parent.snapshot.data.title);
    }

    if (state && parent) {
      data.push(... this.getTitle(state, state.firstChild(parent)));
    }
    if (data.length > 0) {
      return data;
    } else {
      return ['컴플라이 폼팁 호환리스트'];
    }
  }

  getMeta(state, parent) {
    let data = [];
    if (parent && parent.snapshot.data && parent.snapshot.data.meta) {
      let children = [];
      parent.snapshot.data.meta.forEach(metaTag => {
        let propertyName = 'name';
        if (metaTag.hasOwnProperty('property')) {
          propertyName = 'property';
        }

        let is_tag_exist = this.metaService.getTag(propertyName + '="' + metaTag.name + '"');

        if (typeof is_tag_exist != null) {
          this.metaService.removeTag(propertyName + '="' + metaTag.name + '"');
        }
        children.push(metaTag);
      });
      data.push(children);
    }

    if (state && parent) {
      data.push(... this.getMeta(state, state.firstChild(parent)));
    }
    return data;
  }
}
