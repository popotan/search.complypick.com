import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { DocumentComponent } from './document/document.component';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    DocumentComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      {
        path: '',
        component: IndexComponent,
        data: {
          title: 'COMPLY FOAMTIP 소개',
          meta: [
            { property: 'og:title', content: 'COMPLY FOAMTIP 소개' },
            { property: 'og:description', content: '컴플라이 폼팁이 무엇인지 확인해보세요.' },
            { name: 'description', content: '컴플라이 폼팁이 무엇인지 확인해보세요.' },
            { property: 'og:image', content: 'http://search.complypick.com/assets/images/comply-mixed.png' },
            { property: 'og:url', content: 'http://search.complypick.com' }
          ]
        }
      }, {
        path: 'match',
        loadChildren: 'app/match/match.module#MatchModule'
      }, {
        path: 'document/:document_target',
        component: DocumentComponent,
        data: {
          title: 'COMPLY FOAMTIP SERIES',
          meta: [
            { property: 'og:title', content: 'COMPLY FOAMTIP SERIES' },
            { property: 'og:description', content: '컴플라이 폼팁의 장점을 살펴보세요.' },
            { name: 'description', content: '컴플라이 폼팁의 장점을 살펴보세요.' }
          ]
        }
      }
    ])
  ],
  providers: [Title],
  bootstrap: [AppComponent]
})
export class AppModule { }
