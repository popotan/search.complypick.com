from flask import Flask, send_from_directory, session, request, render_template, g
from flask_session import Session
from flask_bcrypt import Bcrypt

# from static_soundpick_net.config import database
from datetime import timedelta

app = Flask(__name__, static_url_path='/static')
app.secret_key = 'static_soundpick_net'

@app.route('/<path:path>')
def send_js(path):
	"""
		static 파일들을 템플릿상에서 url_for 없이 사용하기 위해 라우팅 제공
	"""
	return send_from_directory('static/', path)