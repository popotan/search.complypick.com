from flask import Flask, send_from_directory, session, request, render_template, g
from flask_session import Session
from flask_bcrypt import Bcrypt

from api_search_complypick_com.config import database
from datetime import timedelta

app = Flask(__name__, template_folder='template', static_url_path='/static')
app.secret_key = 'api_search_complypick_com'

app.config.from_object(database)
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()
db.init_app(app)

@app.route('/static/<path:path>')
def send_js(path):
	"""
		static 파일들을 템플릿상에서 url_for 없이 사용하기 위해 라우팅 제공
	"""
	return send_from_directory('static/', path)

@app.after_request
def add_header(response):
    """
    Add headers to both force latest IE rendering engine or Chrome Frame,
    and also to cache the rendered page for 10 minutes.
    """
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response

from api_search_complypick_com.controller.search import search
from api_search_complypick_com.controller.root import root

app.register_blueprint(root, url_prefix='/api')
app.register_blueprint(search, url_prefix='/api/search')