from flask import Blueprint, render_template, jsonify, request, session, redirect
from sqlalchemy import text, desc, or_

from datetime import datetime, timedelta

from api_search_complypick_com.model.comply_brand import comply_brand_orm
from api_search_complypick_com.model.comply_model import comply_model_orm
from api_search_complypick_com.model.eartip import eartip_orm

search = Blueprint('search', __name__)

@search.route('', methods=['GET'])
def keyword_search():
    if request.args.get('keyword'):
        keyword = request.args.get('keyword')
        model_list = get_model_object(keyword)
        model_repr = ['id', 'name', 'parent_id', 'brand', 'description', 'sort_order', 'search_keywords', 'image_file', 'reg_date']
        return jsonify({
            'status' : 200,
            'message' : 'SUCCESS',
            'response' : [{_col : getattr(_row, _col) for _col in model_repr} for _row in model_list]
        })
    else:
        return jsonify({
            'status' : 204,
            'message' : 'NO_KEYOWRD'
        })

def get_model_object(keyword):
    model_id_list = search_brand(keyword) + search_model(keyword)
    model_id_list = list(set(model_id_list))
    print(model_id_list)
    if len(model_id_list):
        model_list = comply_model_orm().query.filter(comply_model_orm.id.in_(model_id_list)).all()
        return model_list
    else:
        return []

def search_brand(keyword):
    brand_list = comply_brand_orm().query\
    .filter(
        or_(
            comply_brand_orm.name.ilike(text('"%' + keyword + '%"')),
            comply_brand_orm.search_keywords.ilike(text('"%' + keyword + '%"'))
            )
        ).all()
    # dist_by_id_arr = list(set([brand.id for brand in brand_list]))
    model_id_arr = []
    for brand in brand_list:
        model_list = comply_model_orm().query.filter_by(parent_id=brand.id).all()
        model_id_arr += [model.id for model in model_list]
    return model_id_arr

def search_model(keyword):
    model_list = comply_model_orm().query\
    .filter(
        or_(
            comply_model_orm.name.ilike(text('"%' + keyword + '%"')),
            comply_model_orm.search_keywords.ilike(text('"%' + keyword + '%"'))
        )
    ).all()
    return [model.id for model in model_list]