from flask import Blueprint, render_template, jsonify, request, session, redirect
from sqlalchemy import text, desc, or_

from datetime import datetime, timedelta

from api_search_complypick_com.model.comply_brand import comply_brand_orm
from api_search_complypick_com.model.comply_model import comply_model_orm
from api_search_complypick_com.model.eartip import eartip_orm

root = Blueprint('root', __name__)

@root.route('/<int:model_id>', methods=['GET'])
def model_info(model_id):
    model = comply_model_orm().query.filter_by(id=model_id).first()
    model_repr = ['id', 'name', 'parent_id', 'brand', 'description', 'sort_order', 'search_keywords', 'image_file', 'eartips', 'reg_date']
    return jsonify({
        'status' : 200,
        'message' : 'SUCCESS',
        'response' : {_col : getattr(model, _col) for _col in model_repr}
    })

@root.route('/<int:model_id>/eartip', methods=['GET'])
def keyword_search(model_id):
    model = comply_model_orm().query.filter_by(id=model_id).first()
    print(model.eartips)
    eartip_list = eartip_orm().query.filter(eartip_orm.id.in_([m for m in model.eartips])).filter_by(is_visible=True).all()
    eartip_repr = ['id', 'name', 'description', 'description_url', 'product_image_file_name', 'description_image_file_name', 'tip_image_file_name', 'nickname', 'href', 'is_visible', 'reg_date']
    return jsonify({
        'status' : 200,
        'message' : 'SUCCESS',
        'response' : [{_col : getattr(_row, _col) for _col in eartip_repr} for _row in eartip_list]
    })