# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from flask_sqlalchemy import SQLAlchemy

from api_search_complypick_com import db

from datetime import datetime, timedelta

class comply_brand_orm(db.Model):
    __bind_key__ = 'eartip'
    __tablename__ = 'brand'
    id = Column(Integer,primary_key=True,unique=True)
    parent_id = Column(Integer)
    name = Column(String)
    description = Column(String)
    sort_order = Column(Integer)
    page_title = Column(String)
    meta_keywords = Column(String)
    meta_description = Column(String)
    layout_file = Column(String)
    is_visible = Column(Boolean)
    search_keywords = Column(String)
    url = Column(String)
    _parent_category_list = Column('parent_category_list', String)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    @property
    def parent_category_list(self):
        return json.loads(self.parent_category_list)

    @parent_category_list.setter
    def parent_category_list(self, arr):
        self._parent_category_list = str(arr)

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()