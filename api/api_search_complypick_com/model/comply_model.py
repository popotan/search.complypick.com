# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from flask_sqlalchemy import SQLAlchemy

from api_search_complypick_com import db

from api_search_complypick_com.model.comply_brand import comply_brand_orm

from datetime import datetime, timedelta

import json

class comply_model_orm(db.Model):
    __bind_key__ = 'eartip'
    __tablename__ = 'model'
    id = Column(Integer,primary_key=True,unique=True)
    parent_id = Column(Integer)
    name = Column(String)
    description = Column(String)
    sort_order = Column(Integer)
    page_title = Column(String)
    meta_keywords = Column(String)
    meta_description = Column(String)
    layout_file = Column(String)
    is_visible = Column(Boolean)
    _parent_category_list = Column('parent_category_list', String)
    image_file = Column(String)
    search_keywords = Column(String)
    url = Column(String)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    _eartips = Column('eartips', String)

    @property
    def brand(self):
        if self.parent_id:
            brand = comply_brand_orm().query.filter_by(id=self.parent_id).first()
            brand_repr = ['id', 'name', 'description', 'reg_date']
            return {_col : getattr(brand, _col) for _col in brand_repr}
        else:
            return None

    @property
    def eartips(self):
        if self._eartips:
            return json.loads(self._eartips.replace(" ",""))
        else:
            return []

    @eartips.setter
    def eartips(self, arr):
        if type(arr) is list:
            if len(arr) > 0:
                temp = "[" + ",".join([str(v) for v in arr]) + "]"
                self._eartips = temp.replace(" ","")
            else:
                self._eartips = []
        else:
            self._eartips = []

    @property
    def parent_category_list(self):
        return json.loads(self.parent_category_list)

    @parent_category_list.setter
    def parent_category_list(self, arr):
        self._parent_category_list = str(arr)

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()