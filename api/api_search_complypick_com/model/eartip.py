# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from flask_sqlalchemy import SQLAlchemy

from api_search_complypick_com import db

from datetime import datetime, timedelta

class eartip_orm(db.Model):
    __bind_key__ = 'eartip'
    __tablename__ = 'tip'
    id = Column(Integer,primary_key=True,unique=True)
    name = Column(String)
    description = Column(String)
    target = Column(String)
    product_image_file_name = Column(String)
    description_image_file_name = Column(String)
    tip_image_file_name = Column(String)
    nickname = Column(String)
    href = Column(String)
    description_url = Column(String)
    is_visible = Column(Boolean, default=False)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()