import { Search.Complypick.ComPage } from './app.po';

describe('search.complypick.com App', () => {
  let page: Search.Complypick.ComPage;

  beforeEach(() => {
    page = new Search.Complypick.ComPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
